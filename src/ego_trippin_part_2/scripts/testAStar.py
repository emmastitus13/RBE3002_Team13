import astar
import rospy, tf, copy, math
from kobuki_msgs.msg import BumperEvent
from geometry_msgs.msg import Twist, Pose, PoseStamped
from tf.transformations import euler_from_quaternion
import numpy as np
from std_msgs.msg import String
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
import Queue as Q
from collections import defaultdict
import unittest


class test(unittest.TestCase):
    # test heuristic calculations
    def testHeurCost(self):
        print("testing heuristic calculation")
        # define a small grid with no obstacles to start testing with
        # Creates a list containing 5 lists, each of 8 items, all set to 0
        x, y = 5, 5;
        grid = [[0 for i in range(x)] for j in range(y)]

        # define start node
        start = astar.Node(0, 0)

        # define end node
        goal = astar.Node(3, 4)

        # test heuristic calculator

        self.assertEqual(start.heuristic_cost_estimate(goal), 5, 0.002)

    # test finding the neighbors of a cell
    def testFindNeighbors(self):
        print("testing finding the neighbors of a cell")
        # define a small grid with no obstacles to start testing with
        # Creates a list containing 5 lists, each of 8 items, all set to 0
        x, y = 5, 5;
        grid = [[0 for i in range(x)] for j in range(y)]

        # define start node
        start = astar.Node(3,3)

        top = astar.Node(3, 4)

        right = astar.Node(4,3)

        bot = astar.Node(3, 2)

        left = astar.Node(2,3)

        neighbors = start.getNeighbors()
        # top neighbor
        self.assertEqual(neighbors[0]._x_pos, 3)
        self.assertEqual(neighbors[0]._y_pos, 4)
        # right neighbor
        self.assertEqual(neighbors[1]._x_pos, 4)
        self.assertEqual(neighbors[1]._y_pos, 3)
        # bottom neighbor
        self.assertEqual(neighbors[2]._x_pos, 3)
        self.assertEqual(neighbors[2]._y_pos, 2)
        # left neighbor
        self.assertEqual(neighbors[3]._x_pos, 2)
        self.assertEqual(neighbors[3]._y_pos, 3)

    # test AStar on a cell out of bounds
    def testAstarFailure(self):
        print("test A star for a cell out of bounds")
        # define a small grid with no obstacles to start testing with
        # Creates a list containing 5 lists, each of 8 items, all set to 0
        x, y = 5, 5;
        grid = [[0 for i in range(x)] for j in range(y)]

        # define start node
        start = astar.Node(0, 0)

        end = astar.Node(32, 42)

        # test Asatr
        self.assertEqual(start.AStar(start,end), "failure, cell out of bounds")

    # test Astar on a goal in the grid
    def testAStarCurrentLocation(self):
        print("test A star for current location")
        # define a small grid with no obstacles to start testing with
        # Creates a list containing 5 lists, each of 8 items, all set to 0
        x, y = 5, 5;
        grid = [[0 for i in range(x)] for j in range(y)]

        start = astar.Node(0, 0)

        end = astar.Node(0, 0)
        # grab the path from the astar return
        goalPath=start.AStar(start,end)[0]

        self.assertEquals(goalPath.__len__(), 0)
        # self.assertTrue(start.nodeComp(goalPath[0]))

    # test Astar on a goal in the grid
    def testAStarOnNeighbor(self):
        print("test A star for neighbor cell")
        # define a small grid with no obstacles to start testing with
        # Creates a list containing 5 lists, each of 8 items, all set to 0
        x, y = 5, 5;
        grid = [[0 for i in range(x)] for j in range(y)]

        # define start node
        start = astar.Node(0, 0)
        end = astar.Node(1, 0)

        goalPath = start.AStar(start, end)[0]
        self.assertEquals(goalPath.__len__(),1)
        self.assertTrue(end.nodeComp(goalPath[0]))

    # test Astar on a goal in the grid
    def testAStarOnRandomCell(self):
        print("test A star for random cell")
        # define a small grid with no obstacles to start testing with
        # Creates a list containing 5 lists, each of 8 items, all set to 0
        x, y = 5, 5;
        grid = [[0 for i in range(x)] for j in range(y)]

        # define start node
        start = astar.Node(0, 0)

        #define goal
        end = astar.Node(3, 3)

        wayPoint1 = astar.Node(1,1)
        wayPoint2 = astar.Node(2,2)

        way1 = False
        way2 = False

        goalPath = start.AStar(start,end)[0]
        for temp in goalPath:
            print(temp._x_pos,'<-x pos',temp._y_pos,'<- ypos')
            if  wayPoint1.nodeComp(temp):
                way1 = True
                return way1
            if  wayPoint2.nodeComp(temp):
                way2 = True
                return way2
        self.assertEquals(goalPath.__len__(), 6)
        self.assertTrue(way1)
        self.assertTrue(way2)
        self.assertTrue(end in start.AStar(start, end))

if __name__ == '__main__':
	try:
		unittest.main()
	except rospy.ROSInterruptException:
		pass