#! /usr/bin/env python
import Queue, math
from collections import defaultdict
import rospy


class Node:
    def __init__(self, x, y):
        self._x_pos = x
        self._y_pos = y

    def setParent(self,parentNode):
        self._parent = parentNode

    # Takes in a node and returns the euclidean distance from itself to the node
    def heuristic_cost_estimate(self, node):
        distance = ((node._x_pos - self._x_pos) ** 2) + ((node._y_pos - self._y_pos) ** 2)
        return math.sqrt(distance)

    def findAngle(self, node):
        return math.atan2((node._y_pos-self._y_pos),(node._x_pos-self._x_pos))

    #returns tuples of neighbors linked with their distance from self
    def getNeighbors(self,occupiedCells, closedset, openCellList):
        neighborList = list()

        # top node
        neighborList.append(Node(self._x_pos, self._y_pos + 1))
        # top right node
        neighborList.append(Node(self._x_pos+1, self._y_pos + 1))
        #right node
        neighborList.append(Node(self._x_pos + 1, self._y_pos))
        # bottom right node
        neighborList.append(Node(self._x_pos+1, self._y_pos - 1))
        # bottom node
        neighborList.append(Node(self._x_pos, self._y_pos - 1))
        # bottom left node
        neighborList.append(Node(self._x_pos-1, self._y_pos - 1))
        # left node
        neighborList.append(Node(self._x_pos - 1, self._y_pos))
        # top left node
        neighborList.append(Node(self._x_pos-1, self._y_pos + 1))

        # now that we have all the neighbrs, check if they are actually an obstacle
        freeNeighborList=[]


        for neigh in neighborList:
            dontAddMe = False
            for cell in occupiedCells:
                if neigh.__cmp__(cell):
                    dontAddMe = True
                    break
            for cell in closedset:
                if neigh.__cmp__(cell):
                    dontAddMe = True
                    break
            for cell in openCellList:
                if neigh.__cmp__(cell[1]):
                    dontAddMe = True
                    break
            if dontAddMe == False:
                freeNeighborList.append(neigh)

        return freeNeighborList

    # compare two nodes for x and y positions
    def nodeComp(self, node2):
        if (self._x_pos == node2._x_pos):
            if (self._y_pos == node2._y_pos):
                return True
        return False

    def AStar(self, destNode, gridcellpub):

        closedSet = set()
        openSet = Queue.PriorityQueue()
        openSet.put((self.heuristic_cost_estimate(destNode), self))
        cameFrom = list()
        gScore = defaultdict(lambda: float('inf'))
        fScore = defaultdict(lambda: float('inf'))
        gScore[self] = 0
        fScore[self] = self.heuristic_cost_estimate(destNode)
        self.setParent(0)

        frontier = []
        path=[]

        gridcellpub.pubNode(frontier,"frontier")
        gridcellpub.pubNode(closedSet,"closedSet")
        gridcellpub.pubNode(path,"path")
        rospy.sleep(1)

        while openSet:
            ## grab a node from the frontier
            currenttuple = openSet.get() # Lock it, Drop it, Shake it, Twist it, Bop it
            # access the current node
            current=currenttuple[1]
            print "x " + str(current._x_pos) + " y " + str(current._y_pos)

            # if we are at the destination
            if current.__cmp__(destNode):
                # figure out what the best path is
                path=(reconstruct_path(current))
                gridcellpub.pubNode(path, "path")
                return path

            # if we are not at the destination
            # add the current node to the closed set
            closedSet.add(current)

            # find all the obstacles and cost map
            costMapVals = gridcellpub.getCostMap()
            occupiedCells= gridcellpub.publishCells()
            occupiedNodes = []
            for tuple in occupiedCells:
                tempNode = Node(tuple[0],tuple[1])
                occupiedNodes.append(tempNode)

            # find all the valid, unvisited, neighbors of the current cell
            openCellList = list(openSet.queue)
            neighbors = current.getNeighbors(occupiedNodes, closedSet, openCellList)
            k = 0
            for neighbor in neighbors:
                # add the neighbor to the open set

                tentativeGScore = gScore[current] + current.heuristic_cost_estimate(neighbor) + costMapVals[k]

                if(tentativeGScore < gScore[neighbor]):
                    neighbor.setParent(current)
                    gScore[neighbor] = tentativeGScore
                    fScore[neighbor] = gScore[neighbor] + neighbor.heuristic_cost_estimate(destNode)
                openSet.put((fScore[neighbor], neighbor))
                k = k + 1
            publishOpen = list(openSet.queue)

            # publish everything
            rospy.sleep(0.05)
            gridcellpub.pubNode(publishOpen, "frontier")
            gridcellpub.pubNode(closedSet, "closedSet")
            gridcellpub.pubNode(current,"current")
            # rospy.sleep(0.05)
        return "failure"

    def AswoleStar(self, destNode, gridcellpub, diameter, Kg=1): #takes in the buffered map

        closedSet = set()
        openSet = Queue.PriorityQueue()
        openSet.put((self.heuristic_cost_estimate(destNode), self))
        cameFrom = list()
        gScore = defaultdict(lambda: float('inf'))
        fScore = defaultdict(lambda: float('inf'))
        gScore[self] = 0
        fScore[self] = self.heuristic_cost_estimate(destNode)
        self.setParent(0)

        frontier = []
        path=[]

        gridcellpub.pubNode(frontier,"frontier")
        gridcellpub.pubNode(closedSet,"closedSet")
        gridcellpub.pubNode(path,"path")
        rospy.sleep(1)

        while openSet:
            ## grab a node from the frontier
            currenttuple = openSet.get() # Lock it, Drop it, Shake it, Twist it, Bop it
            # access the current node
            current=currenttuple[1]
            print "x " + str(current._x_pos) + " y " + str(current._y_pos)

            # if we are at the destination
            if current.__cmp__(destNode):
                # figure out what the best path is
                path=(reconstruct_path(current))
                gridcellpub.pubNode(path, "path")
                return path

            # if we are not at the destination
            # add the current node to the closed set
            closedSet.add(current)

            # find all the obstacles and cost map
            costMapVals = gridcellpub.getCostMap()
            occupiedCells= gridcellpub.addBuffer(diameter)
            occupiedNodes = []
            for tuple in occupiedCells:
                tempNode = Node(tuple[0],tuple[1])
                occupiedNodes.append(tempNode)

            # find all the valid, unvisited, neighbors of the current cell
            openCellList = list(openSet.queue)
            neighbors = current.getNeighbors(occupiedNodes, closedSet, openCellList)
            k = 0
            for neighbor in neighbors:
                # add the neighbor to the open set

                tentativeGScore = gScore[current] + current.heuristic_cost_estimate(neighbor) + costMapVals[k]

                if(tentativeGScore < gScore[neighbor]):
                    neighbor.setParent(current)
                    gScore[neighbor] = tentativeGScore * Kg
                    fScore[neighbor] = gScore[neighbor] + neighbor.heuristic_cost_estimate(destNode)
                openSet.put((fScore[neighbor], neighbor))
                k = k + 1
            publishOpen = list(openSet.queue)

            # publish everything
            gridcellpub.pubNode(publishOpen, "frontier")
            gridcellpub.pubNode(closedSet, "closedSet")
            gridcellpub.pubNode(current,"current")
            # rospy.sleep(0.05)
        return "failure"


    def wayPointCalculator(self,path,gridcellpub):
        # store all the anlges between nodes in a list
        angleList = []
        # store a list of waypoint nodes
        waypointList = []
        waypointList.append(path[0])
        #store a list of waypoints with their angles to drive to
        print (path[0]._x_pos, path[0]._y_pos)
        driveList = []
        try:
            driveList.append([path[0], path[0].findAngle(path[1])])
        except IndexError:
            # in the event there is only one waypoint, set the angle to zero
            driveList.append([path[0], 0])

        # go through the A* path and find the angle between all the nodes
        # print('size of path  ',path.__len__())
        counter = 0
        for node in range(path.__len__()):
            counter = counter + 1
            try:
                angleList.append((path[node].findAngle(path[node+1]),path[node]))
            except IndexError:
                break
        # print('size of counter ',counter)


        # go through the list of angles and find where the angle changes
        for angleTuple in range(angleList.__len__()):
            angle = angleList[angleTuple][0]
            node = angleList[angleTuple][1]
            # try to grab the next angle and node in the list
            try:
                nextTuple = angleList[angleTuple+1]
                nextAngle = nextTuple[0]
                nextNode = nextTuple[1]
            except IndexError:
                # we have reached the last node,dd it as a waypoint
                waypointList.append(node)
                driveList.append([node, angle])
                break

            if angle != nextAngle:
                waypointList.append(nextNode)
                driveList.append([nextNode, nextAngle])
                # print "x " + str(nextNode._x_pos) + " y " + str(nextNode._y_pos) + " theta " + str(nextAngle)

        waypointList.append(path[waypointList.__len__()-1])
        # return the waypoint list
        gridcellpub.pubNode(waypointList, "waypoint")
        # print('size of waypoint list ',waypointList.__len__())
        return driveList


# remake a path based on all
def reconstruct_path(current):
    total_path = []
    while current._parent:
        total_path.append(current)
        current=current._parent
    return total_path[::-1]