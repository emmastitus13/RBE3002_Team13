#! /usr/bin/env python
import rospy, tf, copy, math
from kobuki_msgs.msg import BumperEvent
from geometry_msgs.msg import Twist, Pose, PoseStamped
from tf.transformations import euler_from_quaternion
import numpy as np
from std_msgs.msg import String
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
import Queue as Q
from collections import defaultdict
import unittest
""""
The A* search algorithm is an informed search algorithm of a known or explorable area, meaning that the program knows where the robot is, and where the goal is.  It also works with unexplored maps 
if the robot is equipped to explore and update the map in real time.  This algorithm works on any sort of graph that can be navigated, including directional graphs, but this lab uses a grid, so 
there are many ways to implement it.  Below is some modified pseudocode based on Wikipedia's article on A* (https://en.wikipedia.org/wiki/A*_search_algorithm) which I recommend reading to 
supplement course material.  The algorithm functions by expanding a frontier from the start node until it discovers the goal, in a method that could be described as a prioritized and incomplete 
depth-first search.  All nodes are assigned three values, the F, G, and H scores.  the G score of a node is the best navigable path that the search has found so far.  the H score is a heuristic 
that estimates the best possible distance between the node and the goal.  The F score is the sum of the G and H scores, and represents the best theoretical distance between the start node and the 
goal which travels through the current node.  By iterating the algorithm through the node on the frontier with the best F score, it will find the best possible path.

Authors: Wikipedia, Connor Flanigan
""" 
def makePose(lx=0, ly=0, lz=0, ax=0, ay=0, az=0):
	tempPose = Pose()
	tempPose.position.x = lx
	tempPose.position.y = ly	
	tempPose.position.z = lz
	[qx, qy, qz, qw] = quaternion_from_euler(ax, ay, az)
	tempPose.orientation.x = qx
	tempPose.orientation.y = qy
	tempPose.orientation.z = qz
	tempPose.orientation.w = qw
	return tempPose


def makeNode(x, y, bloke):
	tempPose = makePose(x, y)


class Node:
	# Takes in a coord pose and stores it
	# Parses the pose to store x and y positions
	# Takes a value for node state: 100 for obstacle
	# 0 for explored open, -1 for unexplored
	def __init__(self, coord, state):
		self._coord = coord
		self._x_pos = coord.position.x
		self._y_pos = coord.position.y
		self._state = state

	# Sets the _is_obstacle variable
	def setState(self, state):
		self._state = state


	# Takes in a goal node and returns the distane from itself to the goal
	def getCost(self, goal):
		return math.sqrt((goal._x_pos-self._x_pos)**2+(goal._y_pos-self._y_pos)**2)


class NodeMap:
	def __init__(self):
		# Do stuff
		pass

	# Takes in a pose and condition and stores it as a Node object
	def setInit(self,initialPose,initialState):
		self._initial = Node(initialPose,initialState)


	# Takes in a node and stores a node
	def setGoal(self, goal):
		self._goal = goal


	# Stores the occupancy grid
	def storeGrid(self, grid):
		self._grid = grid


	# Astar navigates from start to goal
	# start is a poseStamped msg
	# goal is a pose msg
	# returns a list of nodes to traverse
	def AStar(self):
		closedset = defaultdict(lambda: 0)  # The set of nodes already evaluated.
											# 0 if not explored, 1 if explored
		frontier = Q.PriorityQueue()			# The set of tentative nodes to be evaluated,
											# initially containing the start node.  The 
											# nodes in this set are the nodes that make 
											# the frontier between the closed set and 
											# all other nodes.
		self._came_from = []						# The map of navigated nodes.

		''' 
		The g_score of a node is the distance of the shortest path from the 
		start to the node. Start by assuming that all nodes that have yet to
		be processed cannot be reached '''
		# g_score = {}
		g_score = defaultdict(lambda: math.inf)
		
		# The starting node has zero distance from start
		g_score[self._initial] = 0
		
		# The f_score of a node is the estimated total cost from start to goal to
		# the goal.  This is the sum of the g_score (shortest known path) and the
		# h_score (best possible path).
		# assume same as g_score
		f_score = defaultdict(lambda: math.inf)
		
		# heuristic_cost_estimate(a, b) is the shortest possible path between 
		# a and b, this can be euclidean, octodirectional, Manhattan, or 
		# something fancy based on how the machine moves the best possible
		# distance between the start and the goal will be the heuristic
		f_score[self._initial] = g_score[self._initial] + self.heuristic_cost_estimate(self._initial)
		
		# Add starting point to priority queue
		frontier.put((f_score[self._initial],self._initial))

		# go through the start node's neighbors
		for neigh in self.getNeighbors(self._initial):
			# record the neighbors in the frontier
			frontier.put((f_score[neigh],neigh))

		# remove visited nodes from the frontier
		frontier.get(self._initial)
		# add visited nodes to the closed set
		closedset[self._initial] = f_score[self._initial]
		# add the start node to the list of parent nodes
		self._came_from.append(self._initial)

		# as long as we have a frontier, and haven't hit the goal
		while (frontier.qsize() > 0):
			# while there are still nodes that have not
			# been checked, continually run the algorithm
			
			# grab a node from the frontier
			currenttuple = frontier.get() # Lock it, Drop it, Shake it, Twist it, Bop it
			# access the current node
			current=currenttuple[1]

			# print(current._x_pos,'<-- cur x position',current._y_pos,'<-- cur y position')
			# print(self._goal._x_pos, '<-- x position', self._goal._y_pos, '<-- y position')

			# if the best possible path found leads to the goal, it is the best 
			# possible path that the robot could discover 
			if self.nodeComp(current, self._goal):
				self._came_from.append(self._goal)
				for obj in sorted(closedset.keys()):
					print (obj._x_pos)
				return self._came_from
			#
			closedset[current] = 1 # Lock it
			try:
				neighbors = self.getNeighbors(current)
			except IndexError:
				return "failure, cell out of bounds"

			# evaluate each neighboring node
			for cast_member in neighbors:

				#check if the current neighbor has been checked before
				if closedset[cast_member] == 1:
					# if it has, do nothing
					pass
				else:
					# we haven't checked this node yet
					# calculate the heuristic so we can add it to the frontier
					f_score[cast_member] = self.heuristic_cost_estimate(cast_member)
					frontier.put((f_score[cast_member],cast_member))
					closedset[cast_member] = 1


				tentative_g_score = g_score[current] + self.dist_between(current,cast_member) # create a new g_score for the current neighbor by adding the g_score from the current node and
																						# the distance to the neighbor

				if tentative_g_score < g_score[cast_member]:                 # if the neighbor has not been evaluated yet, or if a better path to the neighbor has been found,
																									# update the neighbor
					self._came_from.append(current)                                                        # The node to reach this node from in the best time is the current node
					g_score[cast_member] = tentative_g_score                                           # The G score of the node is what we tentatively calculated earlier
					f_score[cast_member] = g_score[cast_member] + self.heuristic_cost_estimate(cast_member) # The F score is the G score and the heuristic
					# if closedset[cast_member]==1 :                                                      # add this neighbor to the frontier if it was not in it already
					# 	frontier.put((f_score[cast_member],cast_member))

		return "failure, ran out of nodes" #if the program runs out of nodes to check before it finds the goal, then a solution does not exist


	# if there were no obstacles in the way of the robot, what is the 
	# shortest path to the goal?  Return that value
	def heuristic_cost_estimate(self,currentNode):
		return currentNode.getCost(self._goal)


	# compare two nodes for x nd y positions
	def nodeComp(self, node1, node2):
		if (node1._x_pos == node2._x_pos):
			if (node1._y_pos == node2._y_pos):
				return True
		return False


	# Returns list of neighboring nodes to input
	def getNeighbors(self, current):

		top_pose = Pose()
		top_pose.position.x = current._x_pos
		top_pose.position.y = current._y_pos+1

		if (self._grid[top_pose.position.x][top_pose.position.y] == 100):
			state = 100
		else:
			state = -1
		top = Node(top_pose, state)

		right_pose = Pose()
		right_pose.position.x = current._x_pos+1
		right_pose.position.y = current._y_pos
		if (self._grid[right_pose.position.x][right_pose.position.y] == 100):
			state = 100
		else:
			state = -1
		right = Node(right_pose, state)

		butt_pose = Pose()
		butt_pose.position.x = current._x_pos
		butt_pose.position.y = current._y_pos-1
		if (self._grid[butt_pose.position.x][butt_pose.position.y] == 100):
			state = 100
		else:
			state = -1
		butt = Node(butt_pose, state)

		left_pose = Pose()
		left_pose.position.x = current._x_pos-1
		left_pose.position.y = current._y_pos
		if (self._grid[left_pose.position.x][left_pose.position.y] == 100):
			state = 100
		else:
			state = -1
		left = Node(left_pose, state)
		return [top, right, butt, left]
		

	def dist_between(self,current,neighbor):
		return 1


	# Starting from the goal, work backwards to find the start.  We recommend returning a path nav_msgs, which is an array of PoseStamped with a header
	# def reconstruct_path(self, current):
    #
	# 	# start by adding goal to the path
	#     total_path = [current]
	#
	# 	# run while reconstruct_path hasn't reached the start
	#     while current in came_from:
	#
	# 		# The current node is now the node that leads to the previous node
	#         current = came_from[current]
	#
	# 		# add the current node to the front of the list
	#         total_path.append(current)
	#
	# 	# The list is now the shortest path from the start to the end
	#     return total_path

class test(unittest.TestCase):
	# test heuristic calculations
	def testHeurCost(self):
		print("testing heuristic calculation")
		#define a small grid with no obstacles to start testing with
		# Creates a list containing 5 lists, each of 8 items, all set to 0
		x, y = 5, 5;
		grid = [[0 for i in range(x)] for j in range(y)]
		
		#define start node
		startPos = Pose()
		startPos.position.x=0
		startPos.position.y=0
		start = Node(startPos,0)

		#define end node
		endPos = Pose()
		endPos.position.x=12
		endPos.position.y=5
		goal = Node(endPos,0)

		#define a test map
		testMap=NodeMap()


		#test heuristic calculator
		testMap.setGoal(goal)
		self.assertEqual(testMap.heuristic_cost_estimate(start),13,0.002)

	# test finding the neighbors of a cell
	def testFindNeighbors(self):
		print("testing finding the neighbors of a cell")
		#define a small grid with no obstacles to start testing with
		# Creates a list containing 5 lists, each of 8 items, all set to 0
		x, y = 5, 5;
		grid = [[0 for i in range(x)] for j in range(y)]
		
		#define start node
		startPos = Pose()
		startPos.position.x=3
		startPos.position.y=3
		start = Node(startPos,0)

		topPos = Pose()
		topPos.position.x=3
		topPos.position.y=4
		top = Node(topPos,-1)

		leftPos = Pose()
		topPos.position.x=2
		topPos.position.y=3
		top = Node(topPos,-1)

		rightPos = Pose()
		topPos.position.x=4
		topPos.position.y=3
		top = Node(topPos,-1)

		botPos = Pose()
		topPos.position.x=3
		topPos.position.y=2
		top = Node(topPos,-1)

		#define a test map
		testMap=NodeMap()
		testMap.storeGrid(grid)
		neighbors=testMap.getNeighbors(start)
		# top neighbor
		self.assertEqual(neighbors[0]._x_pos,3)
		self.assertEqual(neighbors[0]._y_pos,4)
		# right neighbor
		self.assertEqual(neighbors[1]._x_pos,4)
		self.assertEqual(neighbors[1]._y_pos,3)
		# bottom neighbor
		self.assertEqual(neighbors[2]._x_pos,3)
		self.assertEqual(neighbors[2]._y_pos,2)
		# left neighbor
		self.assertEqual(neighbors[3]._x_pos,2)
		self.assertEqual(neighbors[3]._y_pos,3)

	# test AStar on a cell out of bounds
	def testAstarFailure(self):
		print("test A star for a cell out of bounds")
		# define a small grid with no obstacles to start testing with
		# Creates a list containing 5 lists, each of 8 items, all set to 0
		x, y = 5, 5;
		grid = [[0 for i in range(x)] for j in range(y)]

		# define start node
		startPos = Pose()
		startPos.position.x = 0
		startPos.position.y = 0
		start = Node(startPos, 0)

		endPos = Pose()
		endPos.position.x = 32
		endPos.position.y = 42
		end = Node(endPos, 0)

		# create a sample node map and set it up
		testMap = NodeMap()

		testMap.setInit(startPos,start._state)
		testMap.storeGrid(grid)
		testMap.setGoal(end)

		#test Asatr
		self.assertEqual(testMap.AStar(),"failure, cell out of bounds")

	# test Astar on a goal in the grid
	def testAStarCurrentLocation(self):
		print("test A star for current location")
		# define a small grid with no obstacles to start testing with
		# Creates a list containing 5 lists, each of 8 items, all set to 0
		x, y = 5, 5;
		grid = [[0 for i in range(x)] for j in range(y)]

		# define start node
		startPos = Pose()
		startPos.position.x = 0
		startPos.position.y = 0
		start = Node(startPos, 0)

		endPos = Pose()
		endPos.position.x = 0
		endPos.position.y = 0
		end = Node(endPos, 0)

		# create a sample node map and set it up
		testMap = NodeMap()

		testMap.setInit(startPos, start._state)
		testMap.storeGrid(grid)
		testMap.setGoal(end)

		self.assertEquals(testMap.AStar(),"We're at the goal")

	# test Astar on a goal in the grid
	def testAStarOnNeighbor(self):
		print("test A star for neighbor cell")
		# define a small grid with no obstacles to start testing with
		# Creates a list containing 5 lists, each of 8 items, all set to 0
		x, y = 5, 5;
		grid = [[0 for i in range(x)] for j in range(y)]

		# define start node
		startPos = Pose()
		startPos.position.x = 0
		startPos.position.y = 0
		start = Node(startPos, 0)

		endPos = Pose()
		endPos.position.x = 1
		endPos.position.y = 0
		end = Node(endPos, 0)

		# create a sample node map and set it up
		testMap = NodeMap()

		testMap.setInit(startPos, start._state)
		testMap.storeGrid(grid)
		testMap.setGoal(end)

		self.assertEquals(testMap.AStar(), "We're at the goal")

	# test Astar on a goal in the grid
	def testAStarOnRandomCell(self):
		print("test A star for random cell")
		# define a small grid with no obstacles to start testing with
		# Creates a list containing 5 lists, each of 8 items, all set to 0
		x, y = 5, 5;
		grid = [[0 for i in range(x)] for j in range(y)]

		# define start node
		startPos = Pose()
		startPos.position.x = 0
		startPos.position.y = 0
		start = Node(startPos, 0)

		endPos = Pose()
		endPos.position.x = 3
		endPos.position.y = 3
		end = Node(endPos, 0)

		# create a sample node map and set it up
		testMap = NodeMap()

		testMap.setInit(startPos, start._state)
		testMap.storeGrid(grid)
		testMap.setGoal(end)

		self.assertEquals(testMap.AStar(), "We're at the goal")






if __name__ == '__main__':
	# pose1 = Pose()
	# node1 = Node()
	try:
		unittest.main()
	except rospy.ROSInterruptException:
		pass