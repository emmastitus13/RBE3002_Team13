#!/usr/bin/env python

import rospy, tf, copy
from math import cos, sin, tan,  degrees, sqrt, pi
from kobuki_msgs.msg import BumperEvent
from geometry_msgs.msg import Twist, Pose, PoseStamped
from tf.transformations import euler_from_quaternion
import numpy as np
from std_msgs.msg import String


def printPosOrient():
    odom_list.waitForTransform('map', 'base_footprint', rospy.Time(0), rospy.Duration(1.0))
    (position, orientation) = odom_list.lookupTransform('map','base_footprint', rospy.Time(0))
    xPosition=position[0]
    yPosition=position[1]

    odomW = orientation
    q = [odomW[0], odomW[1], odomW[2], odomW[3]]
    roll, pitch, yaw = euler_from_quaternion(q)
    theta = degrees(yaw)
    print "pos x=%f y=%f t=%s" % (xPosition, yPosition, theta)
    

wheel_rad = 3.5 / 100.0 #cm
wheel_base = 23.0 / 100.0 #cm

#drive to a goal subscribed as /move_base_simple/goal
#takes in poseStamped
def navToPose(goal):
    global xPosition
    global yPosition
    global theta
    global pose
    

    goalX = goal.pose.position.x
    goalY = goal.pose.position.y
    goalQuaternion = goal.pose.orientation
    quaternion1 = [goalQuaternion.x, goalQuaternion.y, goalQuaternion.z, goalQuaternion.w]
    roll, pitch, yaw = euler_from_quaternion(quaternion1)
    goalTheta = yaw * (180.0/pi)

    initX = xPosition
    initY = yPosition
    xDist = goalX - initX
    yDist = goalY - initY
    distanceToGo = sqrt(xDist**2 + yDist**2)

    initTheta = theta

    rotateAngle = degrees(tan(yDist/xDist))
    
    print "spin!"
    rotate(rotateAngle)

    print "move!"
    driveStraight(.1, distanceToGo)

    print "spin!"
    rotate(goalTheta)

    print "done"



#This function sequentially calls methods to perform a trajectory.
def executeTrajectory():
    driveStraight(.1,.6)
    rospy.sleep(.15)
    printPosOrient()
    rotate(90)
    rospy.sleep(.15)
    printPosOrient()
    driveStraight(.1, .45)
    rospy.sleep(.15)
    printPosOrient()
    rotate(135)
    printPosOrient()


#This function accepts two wheel velocities and a time interval.
def spinWheels(u1, u2, time):

    global pub

    r = wheel_rad
    b = wheel_base
    #compute wheel speeds
    u = (u1 + u2) / 2.0 #linear V
    w = (u2 - u1) / b #angular omega
    start = rospy.Time().now().secs
    #create movement and stop messages
    move_msg = Twist()
    move_msg.linear.x = u #set linear velocity
    move_msg.angular.z = w #set angular velocity

    stop_msg = Twist()
    stop_msg.linear.x = 0
    stop_msg.angular.z = 0

    while(rospy.Time().now().secs - start < time and not rospy.is_shutdown()): # waits for said time and checks for ctrl C
        pub.publish(move_msg)
    pub.publish(stop_msg)



#This function accepts a speed and a distance for the robot to move in a straight line
def driveStraight(speed, distance):
    global pose
    global pub

    #create movement and stop messages
    move_msg = Twist()
    move_msg.linear.x = speed #set linear velocity
    move_msg.angular.z = 0 #set angular velocity

    stop_msg = Twist()
    stop_msg.linear.x = 0
    stop_msg.angular.z = 0

    initX = pose.position.x
    initY = pose.position.y
    atTarget = False
    i = 0
    #move until position = distance specified
    while (not atTarget and not rospy.is_shutdown()):
        curX = pose.position.x
        curY = pose.position.y
        xDist = curX - initX
        yDist = curY - initY
        curDistance = sqrt(xDist**2 + yDist**2)
        if (curDistance >= distance):
            print ("stopping")
            atTarget = True
            pub.publish(stop_msg)
            rospy.sleep(0.15)
        else:
            print ("[%d] moving" % i)
            pub.publish(move_msg)
            rospy.sleep(0.15)
        i = i + 1


#Accepts an angle and makes the robot rotate around it.
def rotate(angle):
    global odom_list
    global pose
    global pub

    if (angle > 180 or angle<-180):
        print "angle is to large or small"
    vel = Twist()

    # set rotation direction
    error = angle-degrees(pose.orientation.z)
    i = 0
    while ((abs(error) >= 2) and not rospy.is_shutdown()):

        if (error > 1):
            #turn left
            print("[%d] turning left" % i)
            vel.angular.z = .08
        else: #assume its less and turn right
            print("[%d] turning right" % i)
            vel.angular.z = -.08

        i = i+1
        pub.publish(vel)
        rospy.sleep(0.15)
        error = angle-degrees(pose.orientation.z)



    vel.angular.z = 0.0
    pub.publish(vel)
    rospy.sleep(0.15)




#This function works the same as rotate how ever it does not publish linear velocities.
def driveArc(radius, speed, angle):
    pass  # Delete this 'pass' once implemented



#Bumper Event Callback function
def readBumper(msg):
    print "Got bumper message b=%d:s=%d" % (msg.bumper, msg.state)
    if ((msg.state == BumperEvent.PRESSED) and (msg.bumper == BumperEvent.CENTER)):
        print "Bumper %d pressed" % msg.bumper
        executeTrajectory()



# (Optional) If you need something to happen repeatedly at a fixed interval, write the code here.
# Start the timer with the following line of code:
#   rospy.Timer(rospy.Duration(.01), timerCallback)
def timerCallback(event):
    global pose
    global xPosition
    global yPosition
    global theta

    odom_list.waitForTransform('map', 'base_footprint', rospy.Time(0), rospy.Duration(1.0))
    (position, orientation) = odom_list.lookupTransform('map','base_footprint', rospy.Time(0))
    pose.position.x=position[0]
    pose.position.y=position[1]

    xPosition = position[0]
    yPosition = position[1]
    
    odomW = orientation
    q = [odomW[0], odomW[1], odomW[2], odomW[3]]
    roll, pitch, yaw = euler_from_quaternion(q)
    #convert yaw to degrees
    pose.orientation.z = yaw
    theta = degrees(yaw)


# This is the program's main function
if __name__ == '__main__':
    # Change this node name to include your username
    rospy.init_node('lhigham_lab2')

    # These are global variables. Write "global <variable_name>" in any other function
    #  to gain access to these global variables

    global pub
    global pose
    global odom_tf
    global odom_list
    pose = Pose()

    pub = rospy.Publisher('cmd_vel_mux/input/teleop', Twist, None, queue_size=10) #robot motion publisher
    bumper_sub = rospy.Subscriber('move_base_simple/events/bumper', BumperEvent, readBumper, queue_size=1) #bumper subscriber
    goal_sub = rospy.Subscriber('move_base_simple/goal', PoseStamped, navToPose, queue_size=1) #navPose subscriber



    rospy.Timer(rospy.Duration(.01, 0), timerCallback)

    # Use this object to get the robot's Odometry
    odom_list = tf.TransformListener()

    # Use this command to make the program wait for some seconds
    rospy.sleep(rospy.Duration(2, 0))




    print "Starting Lab 2"

    printPosOrient()
    #spinWheels(.5, .5, 2)
    #printPosOrient()
    #driveStraight(.1,3)
    #printPosOrient()
    #rotate(-90)
    #printPosOrient()
    #executeTrajectory()
    #rospy.sleep(rospy.Duration(5, 0))

    while not rospy.is_shutdown():
        rospy.spin()

    printPosOrient()

    print "Lab 2 complete!"

