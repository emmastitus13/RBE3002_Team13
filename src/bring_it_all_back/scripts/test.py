#!/usr/bin/env python
from lab5_grid_cells import GridCellPub
import astar
import random
import Queue, math
from collections import defaultdict
import rospy
from math import pi
import numpy
from nav_msgs.msg import GridCells
from geometry_msgs.msg import Point
from map import Map
import Robot

# make a grid of randomly distributed -1 and 0 values
def makeTestGrid(gridCells, startX, startY):
    k = 0
    unknownCells = GridCells()
    unknownCells.header.frame_id = 'map'
    unknownCells.cell_width = gridCells._resolution
    unknownCells.cell_height = gridCells._resolution

    knownCells = GridCells()
    knownCells.header.frame_id = 'map'
    knownCells.cell_width = gridCells._resolution
    knownCells.cell_height = gridCells._resolution

    dataTuple = ()
    print('entering for loop')
    # create a gridcell of nodes with random distribution of -1 and 0 values
    for i in range(0, gridCells._height):  # height should be set to height of grid
        for j in range(0, gridCells._width):  # width should be set to width of grid
            # generate a random number [0 = known] [-1 = unknown]
            value = random.randint(-1, 3)
            if value >= 0:
                value = 0
            # if i is starting y position and j is starting x position,
            # set the value to known
            if j is startX and i is startY:
                value = 0
            # store this value in the map data
            dataTuple = dataTuple + (value,)

            # store the point
            point = Point()
            point.x = (j * gridCells._resolution) + gridCells._offsetX + (
                    .5 * gridCells._resolution)  # added secondary offset
            point.y = (i * gridCells._resolution) + gridCells._offsetY + (
                    .5 * gridCells._resolution)  # added secondary offset
            point.z = 0

            # store the point as known or unknown appropriately
            if value == -1:
                unknownCells.cells.append(point)
                # print(unknownCells.cells)
            elif value == 0:
                knownCells.cells.append(point)
            else:
                pass

            k = k + 1

    gridCells._mapData = dataTuple
    # publish the known and unknown cells
    print('publishing cells')
    gridCells._unknown.publish(unknownCells)
    gridCells._known.publish(knownCells)
    return gridCells

def testFindUnknown(initGrid):
    startnode = astar.Node(1,1)
    initGrid.pubNode(startnode, "current")
    grid = makeTestGrid(initGrid,startnode._x_pos,startnode._y_pos)
    map = Map(1, startnode)
    nodeRichard = map.identifyCells(grid)
    print(startnode)
    truple = map.grabUnexploredNeighbors(nodeRichard, startnode)
    cluster = grid.pubNode(truple[0], "cluster")
    print(truple[0], "output  ", truple[1], "good or bad")

def testBreadthFirstSearch(initGrid):
    startnode = astar.Node(1, 1)
    initGrid.pubNode(startnode, "current")
    grid = makeTestGrid(initGrid,startnode._x_pos,startnode._y_pos)
    map = Map(1, startnode)
    map.identifyCells(grid)
    map._clusterOpenSet.append(startnode)
    grid._buffer = []
    truple = map.breadthFirst(grid)
    grid.pubNode(truple[0], "cluster")

def testClusterMaker(initGrid):
    startnode = astar.Node(1, 1)
    initGrid.pubNode(startnode, "current")
    grid = makeTestGrid(initGrid, startnode._x_pos, startnode._y_pos)
    map = Map(1, startnode)
    map.identifyCells(grid)
    map._clusterOpenSet.append(startnode)
    grid._buffer = []
    truple = map.breadthFirst(grid)
    cluster = map.clusterMaker(truple, startnode)
    grid.pubNode(cluster, "cluster")

def testFindBiggestCluster(initGrid):
    startnode = astar.Node(1, 1)
    initGrid.pubNode(startnode, "current")
    grid = makeTestGrid(initGrid, startnode._x_pos, startnode._y_pos)
    map = Map(1, startnode)
    map.identifyCells(grid)
    grid._buffer = []
    [centroid, cluster, doesExist] = map.clusterCompare(grid, startnode)
    print(doesExist, "does it exist?")
    grid.pubNode(cluster, "cluster")
    grid.pubNode(centroid, "path")

def testDriving(initGrid):
    curRob = Robot.Robot(23)
    rospy.sleep(2)
    while not initGrid._flag1:
        curRob.startingPosition()

    initGrid.__init__()
    initGrid.clearRVIZ()
    rospy.sleep(2)  # allowing time for publishers, subscribers, etc.
    initGrid.addBuffer(23)
    rospy.sleep(1)
    [x,y]=curRob.startingPosition()
    print(x,y)
    targx = x+.5
    targy = y + .5
    curRob.driveToGoal(initGrid,targx,targy)

def testFindingGoal(initGrid):
    startnode = astar.Node(1, 1)
    initGrid.pubNode(startnode, "current")
    grid = makeTestGrid(initGrid, startnode._x_pos, startnode._y_pos)
    map = Map(1, startnode)
    map.identifyCells(grid)
    grid._buffer = []
    [centroid, cluster, doesExist] = map.clusterCompare(grid, startnode)
    cluster2 = map.findNearestKnown(centroid)
    print(doesExist, "does it exist?")
    grid.pubNode(cluster, "cluster")
    grid.pubNode(centroid, "path")
    grid.pubNode(cluster2, "frontier")
