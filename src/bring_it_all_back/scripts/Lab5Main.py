#!/usr/bin/env python
import rospy
from nav_msgs.msg import GridCells
from std_msgs.msg import String
from geometry_msgs.msg import Twist, Point, Pose, PoseStamped, PoseWithCovarianceStamped
from nav_msgs.msg import Odometry, OccupancyGrid
from kobuki_msgs.msg import BumperEvent
import tf
import numpy
import math
import rospy, tf, numpy, math
from lab5_grid_cells import GridCellPub
import astar
from Robot import Robot
from tf.transformations import quaternion_from_euler
import test
from map import Map

#main handler for final
def finalRun():

    theGrid = GridCellPub()


    #turned values for A* algorithm
    Kg = 1.
    Kcm = 1.

    keepGoing = True

    wheel_base = 6.0/100.0 #centimeters

    mrRoboto = Robot(wheel_base)
    mrRoboto._move_base.wait_for_server(rospy.Duration(60))
    mrRoboto._move_base.cancel_all_goals()
    mrRoboto.driveStraight(.2, .1)
    mrRoboto.rotate360()

    while not theGrid._flag1:
        mrRoboto.startingPosition()

    theGrid.__init__()
    theGrid.clearRVIZ()
    rospy.sleep(2)  # allowing time for publishers, subscribers, etc.
    theGrid.addBuffer(wheel_base)
    rospy.sleep(1)


    while keepGoing and not rospy.is_shutdown():
        mrRoboto.startingPosition()
        rospy.sleep(2.)
        robXpos, robYpos, robAngle = theGrid.getStartPos()
        print "x " + str(robXpos) + " y " + str(robYpos)

        start = astar.Node(robXpos, robYpos, robAngle)

        theGrid.pubNode(start, "current")
        map = Map(1, start)
        map.identifyCells(theGrid)
        [centroid, cluster, doesExist, timedout] = map.clusterCompare(theGrid, start, 120)
        if doesExist:
            keepGoing = True
            theGrid.pubNode(cluster, "cluster")
            theGrid.pubNode(centroid, "path")

            print('about to drive')
            flag = mrRoboto.driveToGoal(theGrid,centroid._x_pos, centroid._y_pos)
            if flag:
                print("Drove to the centroid correctly")
                mrRoboto.rotate360()
            else:
                theGrid.addBuffer(mrRoboto._wheelbase)
                print("Driving Failed, find new centroid")
        else:
            print('womperino, no cluster to be found')
            keepGoing = False



#Main handler of the project
def run():
    #init() # idk man
    rospy.init_node('lab5')

    Kg = .8
    Kcm = .4

    wheel_base = 50.0/100.0 #centimeters

    mrRoboto = Robot(wheel_base)
    theGrid = GridCellPub()
    theGrid.__init__()
    rospy.sleep(2) #allowing time for publishers, subscribers, etc.
    theGrid.addBuffer(wheel_base)
    rospy.sleep(1)
    # print("waiting for the start and end")

    while not theGrid._flag1 or not theGrid._flag2:
        pass

    # print ("initial pose and nav goal saved")
    robXpos, robYpos, robAngle = theGrid.getStartPos()
    print "x " + str(robXpos) + " y " + str(robYpos)


    start = astar.Node(robXpos, robYpos, robAngle)
    goalX = theGrid._goalX
    goalY = theGrid._goalY
    goalTheta = theGrid._goalAngle

    # goalList = theGrid.identifyCells(robXpos, robYpos)
    # goal = goalList[0]
    # [goalX, goalY] = [goal[0], goal[1]]

    print "x " + str(goalX) + " y " + str(goalY)
    rospy.sleep(2)
    goal = astar.Node(goalX, goalY)
    # start = astar.Node(0, 0)
    # goal = astar.Node(219, 206)
    curWaypointPath = start.AswoleStar(goal, theGrid, mrRoboto._wheelbase, Kg, Kcm)
    if curWaypointPath.__len__() > 3:
        node = curWaypointPath[2]
    else:
        node = curWaypointPath[1]

    while not rospy.is_shutdown() and not node.__cmp__(goal):
        rospy.sleep(2)
        driveList = []

        stamp = PoseStamped()
        stamp.header.frame_id = "map"
        stamp.header.stamp = rospy.Time()
        pose = Pose()
        [pose.position.x,pose.position.y] = theGrid.gridToMeters(node._x_pos,node._y_pos)
        quat = quaternion_from_euler(0,0,node._angle)
        pose.orientation.x = quat[0]
        pose.orientation.y = quat[1]
        pose.orientation.z = quat[2]
        pose.orientation.w = quat[3]
        stamp.pose = pose
        driveList.append(stamp)

        try:
            print('about to drive')
            mrRoboto._drive.publish(stamp)
            print('insert blocking code here')
            mrRoboto.holdTillGoal()
            print('consider that code blocked')
            rospy.sleep(3)
        except Exception as driveErrors:
            print(driveErrors.args)
            rospy.sleep(3)

        # update the current node based on the current robot position
        [nodeX,nodeY] = theGrid.meterToGrid(mrRoboto._x_pos,mrRoboto._y_pos)
        node = astar.Node(nodeX,nodeY)
        rospy.sleep(2)
        curWaypointPath = node.AswoleStar(goal, theGrid, mrRoboto._wheelbase,Kg, Kcm)

        if curWaypointPath.__len__() > 3:
            node = curWaypointPath[2]
        else:
            node = curWaypointPath[1]
    print('goal reached :)')

def test_AllTheThings():
    wheel_base = 23.0/100.0 #centirmeters
    mrRoboto = Robot(wheel_base)
    print "Click around to make the robot drive"
    while not rospy.is_shutdown():
        pass

def test_driveStraight():
    drive = rospy.Publisher('move_base_simple/goal', PoseStamped, queue_size=1)
    wheel_base = 23.0 / 100.0  # centimeters
    domoArigato = Robot(wheel_base)
    domoArigato.driveStraight(0.2, 5)


if __name__ == '__main__':
    rospy.init_node('lab5')
    rospy.sleep(2)
    try:
        finalRun()
        #run()
        # theGrid = GridCellPub()
        # theGrid.__init__()
        # rospy.sleep(2)
        #theGrid.pubNode([],"cluster")
        # test.makeTestGrid(theGrid)
        # test.testFindUnknown(theGrid)
        # test.testBreadthFirstSearch(theGrid)
        # test.testClusterMaker(theGrid)
        # test.testFindBiggestCluster(theGrid)
        # test.testDriving(theGrid)
        # test.testFindingGoal(theGrid)
    except rospy.ROSInterruptException:
        pass
