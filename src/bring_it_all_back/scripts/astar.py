#! /usr/bin/env python
import Queue, math
from collections import defaultdict
import rospy
from math import pi
import numpy
from nav_msgs.msg import GridCells
from geometry_msgs.msg import Point
from copy import deepcopy


class Node:
    def __init__(self, x, y,angle=0):
        self._x_pos = x
        self._y_pos = y
        self._angle = angle
        self._cost = -99
        self._pos = (x, y)

    def setParent(self,parentNode):
        self._parent = parentNode

    def setAngle(self,angle):
        self._angle = angle

    def setCost(self,cost):
        self._cost = cost

    # Takes in a node and returns the euclidean distance from itself to the node
    def heuristic_cost_estimate(self, node):
        distance = ((node._x_pos - self._x_pos) ** 2) + ((node._y_pos - self._y_pos) ** 2)
        return math.sqrt(distance)

    def findAngle(self, node):
        return math.atan2((node._y_pos-self._y_pos),(node._x_pos-self._x_pos))


    #made in the pursuit of hashability
    def __eq__(self, node2):
        if (self._x_pos == node2._x_pos):
            if (self._y_pos == node2._y_pos):
                return True
        return False

    def __ne__(self, node2):
        return not self.__eq__(node2)

    def __hash__(self):
        return (self._x_pos * self._y_pos) + (self._x_pos - self._y_pos)


    def AswoleStar(self, destNode, gridcellpub, diameter, Kg=1, buffer = True, Kcm = 1): #takes in the buffered map

        closedSet = set()
        openSet = Queue.PriorityQueue()
        openSet.put((self.heuristic_cost_estimate(destNode), self))
        cameFrom = list()
        gScore = defaultdict(lambda: float('inf'))
        fScore = defaultdict(lambda: float('inf'))
        gScore[self] = 0
        fScore[self] = self.heuristic_cost_estimate(destNode)
        self.setParent(0)

        frontier = []
        path=[]

        gridcellpub.pubNode(frontier,"swoleFrontier")
        gridcellpub.pubNode(closedSet,"closedSet")
        gridcellpub.pubNode(path,"path")
        rospy.sleep(1)

        while openSet:
            ## grab a node from the frontier
            currenttuple = openSet.get() # Lock it, Drop it, Shake it, Twist it, Bop it
            # access the current node
            current=currenttuple[1]
            # print "x " + str(current._x_pos) + " y " + str(current._y_pos)

            # if we are at the destination
            if current.__eq__(destNode):
                # set the angle of the current node to the angle of the destination node
                current._angle = destNode._angle
                # figure out what the best path is
                path=(self.reconstruct_path(current,gridcellpub,diameter))
                gridcellpub.pubNode(path, "path")
                gridcellpub.pubNode(self._waypointList, "waypoint")
                return self._waypointList

            # if we are not at the destination
            # add the current node to the closed set
            closedSet.add(current)

            # find all the obstacles and cost map
            costMapVals = gridcellpub.getCostMap()
            if buffer:
                occupiedCells= gridcellpub.addBuffer(diameter)
            else:
                occupiedCells= gridcellpub.publishCells()

            occupiedNodes = []
            for tuple in occupiedCells:
                tempNode = Node(tuple[0],tuple[1])
                occupiedNodes.append(tempNode)

            self._occupiedNodes = occupiedNodes

            # find all the valid, unvisited, neighbors of the current cell
            openCellList = list(openSet.queue)
            neighbors = current.getNeighbors(occupiedNodes, closedSet, openCellList)
            k = 0
            for neighbor in neighbors:
                # add the neighbor to the open set

                tentativeGScore = gScore[current] + current.heuristic_cost_estimate(neighbor) + costMapVals[k] * Kcm

                if(tentativeGScore < gScore[neighbor]):
                    neighbor.setParent(current)
                    gScore[neighbor] = tentativeGScore * Kg
                    fScore[neighbor] = gScore[neighbor] + neighbor.heuristic_cost_estimate(destNode)
                openSet.put((fScore[neighbor], neighbor))
                k = k + 1
            publishOpen = list(openSet.queue)

            # publish everything
            gridcellpub.pubNode(publishOpen, "swoleFrontier")
            gridcellpub.pubNode(closedSet, "closedSet")
            gridcellpub.pubNode(current,"current")
            # rospy.sleep(0.05)
        return "failure"

    # returns tuples of neighbors linked with their distance from self
    def getNeighbors(self, occupiedCells, closedset, openCellList):
        neighborList = list()

        # top node
        neighborList.append(Node(self._x_pos, self._y_pos + 1, pi / 2))
        # top right node
        neighborList.append(Node(self._x_pos + 1, self._y_pos + 1, pi / 4))
        # right node
        neighborList.append(Node(self._x_pos + 1, self._y_pos, 0))
        # bottom right node
        neighborList.append(Node(self._x_pos + 1, self._y_pos - 1, -pi / 4))
        # bottom node
        neighborList.append(Node(self._x_pos, self._y_pos - 1, -pi / 2))
        # bottom left node
        neighborList.append(Node(self._x_pos - 1, self._y_pos - 1, -3 * pi / 4))
        # left node
        neighborList.append(Node(self._x_pos - 1, self._y_pos, pi))
        # top left node
        neighborList.append(Node(self._x_pos - 1, self._y_pos + 1, 3 * pi / 4))

        # now that we have all the neighbrs, check if they are actually an obstacle
        freeNeighborList = []

        for neigh in neighborList:
            dontAddMe = False
            if neigh in self._occupiedNodes:
                dontAddMe = True

            if neigh in closedset:
                dontAddMe = True

            if neigh in openCellList:
                dontAddMe = True

            if neigh._cost > 70 or neigh._cost < 0:
                dontAddMe = True

            if dontAddMe == False:
                freeNeighborList.append(neigh)

        return freeNeighborList

    # remake a path, and construct waypoint list
    def reconstruct_path(self,current,gridObject,diameter):
        total_path = []
        waypoint = [current]
        refNode = current
        while current._parent:
            total_path.append(current)
            # find the distance in meters between the reference node and the current node
            [refX,refY]=gridObject.gridToMeters(refNode._x_pos,refNode._y_pos)
            [curX, curY] = gridObject.gridToMeters(current._x_pos, current._y_pos)
            distance = math.hypot(curX-refX,curY-refY)
            [selfX, selfY]= gridObject.gridToMeters(self._x_pos,self._y_pos)
            distance2 = math.hypot(curX-selfX, curY-selfY)
            # check if the angle between two path nodes has changed
            # check if the distance between two path nodes is greater than the radius of the robot
            if not numpy.isclose(current._angle,current._parent._angle,0.001,0.001,False)\
                 and distance>diameter/2 and distance2>diameter/2:
                # if so, add the current node to the list of waypoints
                waypoint.append(current)
                refNode = current
            # perform the check again with the parent
            current=current._parent
        # add the start position to the waypoint list
        waypoint.append(self)
        # flip both lists, because they were built backwards
        self._waypointList = waypoint[::-1]
        return total_path[::-1]
