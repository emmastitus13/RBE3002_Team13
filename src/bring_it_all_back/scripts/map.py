#!/usr/bin/env python
from astar import Node
import rospy
from nav_msgs.msg import GridCells
from geometry_msgs.msg import Point
from copy import deepcopy
import Queue, math
from collections import defaultdict
import rospy
from math import pi
import numpy

class Map:
    def __init__(self, key, specialNode):
        self._clusterClosedSet = set()
        self._clusterOpenSet = []
        self._key = key
        self._startNode = specialNode

    def __eq__(self, other):
        return self._key == other._key

    def __ne__(self, other):
        return not self.__eq__(other)

    def __hash__(self):
        return hash(self._key)


    # returns a list of x y and cost of each cell in the map
    # publishes the list of unknown cells to GridCells
    def identifyCells(self, gridCells):
        # print "identifying the frontier"

        k = 0
        cells = GridCells()
        cells.header.frame_id = 'map'
        cells.cell_width = gridCells._resolution
        cells.cell_height = gridCells._resolution

        # print str(gridCells._width) + str(gridCells._height)
        identifiedCells = {(i, j): 0 for i in range(gridCells._height) for j in range(gridCells._width)}

        for i in range(0, gridCells._height):  # height should be set to height of grid
            for j in range(0, gridCells._width):  # width should be set to width of grid
                # print k # used for debugging
                # this point is occupied (x,y)=(j,i)
                # record this point as an occupied cell
                tempNode = Node(j,i)
                tempNode._cost = gridCells._mapData[k]
                identifiedCells[j,i] = tempNode

                # publish this point to rviz if unknown
                if (gridCells._mapData[k] == -1):
                    point = Point()
                    point.x = (j * gridCells._resolution) + gridCells._offsetX + (.5 * gridCells._resolution)  # added secondary offset
                    point.y = (i * gridCells._resolution) + gridCells._offsetY + (.5 * gridCells._resolution)  # added secondary offset
                    point.z = 0
                    cells.cells.append(point)

                k = k + 1
                # print('y= ',i,' x= ',j)
        # run A* on the grid
        gridCells._unknown.publish(cells)
        cellsNew = numpy.reshape(gridCells._mapData, (gridCells._width, gridCells._height))
        self._nodeRichard = identifiedCells
        return identifiedCells

    # inputs: gridCellPublisher, dictionary of nodes
    # outputs: centroid, largest valid unknown cluster in dictionary
    # purpose: call breadth first search to find clusters, compare to find the biggest
    def clusterCompare(self, gridcellpub, aNode, timeout=120):
        # initialize the closed set and the frontier
        clusterClosedSet = self._clusterClosedSet
        clusterOpenSet = self._clusterOpenSet
        # decide on minimum valid cluster size
        clusterSize = 15

        # initialize the cluster
        cluster = []
        doesExist = True
        goalNode = Node(0,0)

        # add the starting node to the openset
        clusterOpenSet.append(aNode)

        # keep running as long as there is a frontier
        timedout = False
        timeoutTime = rospy.get_time() + timeout
        # print('current time: ', rospy.get_time(), 'desired time: ', timeoutTime)
        while clusterOpenSet:
            # print('IN THE OPEN SET')
            if rospy.get_time() >= timeoutTime:
                # print('Search time out!')
                timedout = True
                break
            # find the first set of valid unknown neighbors
            try:
                neighborTruple = self.breadthFirst(gridcellpub)
                # print(neighborTruple, 'NEIGHBOR TRUPLEEEEEE')
            except Exception:
                # print (Exception, 'NO BREADTH FIRST')
                doesExist = False
                break
            # find their cluster
            tempCluster = self.clusterMaker(neighborTruple, gridcellpub)
            # print(tempCluster, 'TEMP')
            # if this cluster is big enough, save it
            if tempCluster.__len__() > clusterSize:
                doesExist = True
                cluster = tempCluster
                # print(cluster, 'SAVED CLUSTER')
                clusterSize = tempCluster.__len__()
                # print(clusterSize, 'SAVED SIZE')
                gridcellpub.pubNode(cluster, "bigCluster")

            # step through the cluster we just found
            for node in tempCluster:
                # remove each node from the frontier
                # add each node to the closed set
                clusterClosedSet.add(node)
                # clusterOpenSet.remove(node)

        if doesExist:
            # print('current time: ', rospy.get_time())
            # initialize the max and min coords for the cluster
            nodeMinX = cluster[0]._x_pos
            nodeMinY = cluster[0]._y_pos
            nodeMaxX = cluster[0]._x_pos
            nodeMaxY = cluster[0]._y_pos

            # step through the largest cluster
            for node in cluster:
                # calculate the centroid
                if node._x_pos < nodeMinX:
                    nodeMinX = node._x_pos
                elif node._x_pos > nodeMaxX:
                    nodeMaxX = node._x_pos

                if node._y_pos < nodeMinY:
                    nodeMinY = node._y_pos
                elif node._y_pos > nodeMaxY:
                    nodeMaxY = node._y_pos

            avgX = int((nodeMaxX + nodeMinX)/2)
            avgY = int((nodeMaxY + nodeMinY)/2)

            goalNode = Node(avgX,avgY)

        doesExist = True
        # find the known node closest to the centroid
        goalNode = self.findNearestKnown(goalNode)
        # return the centroid and the cluster
        returnVar = [goalNode, cluster, doesExist, timedout]
        gridcellpub.pubNode(goalNode, "path")
        gridcellpub.pubNode(cluster, "cluster")
        return returnVar

    # generate a cluster of unknown nodes, based on one unknown node
    def clusterMaker(self, neighborTruple, gridcellpub):
        # save the unknown nodes we just found in a local frontier
        current = neighborTruple[0][0]
        localFrontier = []
        localFrontier.append(current)

        clusterClosedSet = self._clusterClosedSet
        clusterOpenSet = self._clusterOpenSet

        # initialize the cluster
        cluster = []
        while len(localFrontier)>0:
            # grab the first value in the frontier
            current = localFrontier[0]
            # remove the current from the open set
            localFrontier.remove(current)
            # add the current node to the closed set
            clusterClosedSet.add(current)
            # expand out to find the next unexplored node in the cluster
            newTruple = self.grabUnexploredNeighbors(gridcellpub._buffer, current, True)
            gridcellpub.pubNode(localFrontier, "local")
            # rospy.sleep(0.05)
            # print(current._cost,'Here is the current')
            # if we did find another node in the frontier,
            if newTruple[1] == True:
                # add the current node to the cluster
                cluster.append(current)
                # print(localFrontier.__len__(), "local frontier size")
                # print (clusterOpenSet.count(current), 'I DONT KNOW BEN I REALLY DONT')
                # publish the current cluster
                gridcellpub.pubNode(cluster, "cluster")
                # rospy.sleep(.05)

                # save the neighbors of the good node in a list
                nodeList = newTruple[2]
                # iterate through the list of neighbors
                # print(len(nodeList),'Node lists length')
                for node in nodeList:
                    # print(node, node._cost, 'Node and node cost')
                    if node not in self._clusterOpenSet:
                        if node._cost is 0:
                            # add all known neighbors to the global frontier
                            self._clusterOpenSet.append(node)
                            gridcellpub.pubNode(self._clusterOpenSet, "frontier")
                    else:
                        self._clusterOpenSet.remove(node)
                        # pass
                    if node._cost is -1 and node not in localFrontier:
                        # print(node, 'adding to frontier**')
                        localFrontier.append(node)
        # we have gone through the entire frontier, and found the full cluster
        # add the local frontier to the gloabl frontier
        clusterOpenSet.extend(localFrontier)
        # return the cluster
        return cluster

    #finds a -1 node in the map using breath-first search from the node its called on
    def breadthFirst(self, gridcellpub):
        clusterClosedSet = self._clusterClosedSet
        clusterOpenSet = self._clusterOpenSet

        # add the current node to the frontier
        gridcellpub.pubNode(clusterOpenSet, "frontier")
        gridcellpub.pubNode(clusterClosedSet, "closedSet")
        # rospy.sleep(0.05)
        # run while the frontier exists
        while clusterOpenSet:
            # grab the first node from the frontier
            current = clusterOpenSet[0]  # Lock it, Drop it, Shake it, Twist it, Bop it
            clusterOpenSet.remove(current)
            # add the current node to the closed set
            clusterClosedSet.add(current)
            # publish the current node for debugging purposes
            gridcellpub.pubNode(current,"current")
            # check if the current cell is valid
            # todo: update to account for walls
            getTheNeighbors = self.grabUnexploredNeighbors(gridcellpub._buffer, current, False)
            # print('herp derp im a dumb robot')
            # if the current node is valid
            if getTheNeighbors[1] == True:
                # return it
                # print (getTheNeighbors, 'printing something else')
                return getTheNeighbors
            else:
                # if the current node is not valid, add the neighbors to the frontier
                allNeighbors=(getTheNeighbors[0])
                # print(allNeighbors,'--------------------')
                clusterOpenSet.extend(allNeighbors)

            gridcellpub.pubNode(clusterOpenSet, "frontier")
            gridcellpub.pubNode(clusterClosedSet, "closedSet")
            gridcellpub.pubNode(current, "current")
            # rospy.sleep(.05)
        raise Exception('no suitable unknown nodes in map')


    #gets the list of neighbors
    def grabUnexploredNeighbors(self, occupiedCells, aNode, inCluster = False):
        # print(occupiedCells)
        clusterClosedSet = self._clusterClosedSet
        clusterOpenSet = self._clusterOpenSet
        neighborList = []
        freeNeighborList = []

        tupleList = [(-1, 1), (0, 1), (1, 1), (1, 0), (1, -1), (0, -1), (-1, -1), (-1, 0)]

        for tuple in tupleList:
            # print(tuple)
            try:
                # print self._x_pos
                # print (nodeDictionary[self._x_pos,self._y_pos])
                neighborList.append(self._nodeRichard[aNode._x_pos+tuple[0],aNode._y_pos+tuple[1]])
                # clusterOpenSet.append(nodeDictionary[aNode._x_pos+tuple[0],aNode._y_pos+tuple[1]])
            except IndexError and KeyError:
                # print(KeyError)
                pass

        goodToReturn = False
        allUnknown = True
        for neigh in neighborList:
            # print('this many neighbors', neighborList.__len__())
            # print('cost of current neighbor: ', neigh._cost)
            # check if the neighbor is explored
            if neigh._cost <= 70 and neigh._cost >= 0:
                # print('the node cost is smol')
                goodToReturn = True
                allUnknown = False

            # check if the neighbor is
            #   an occupied cell,
            #   in the closed set
            #   in the open set
            dontAddMe = False
            # print('checking the occupied cells')
            if neigh._pos in occupiedCells:
                # print(neigh, 'occupado')
                dontAddMe = True

            # print('checking the cluster')
            # when we are looking for a cluster, it's okay to dip into the global frontier
            if inCluster:
                pass
                # if neigh._cost is 0:
                #     dontAddMe = True
                # print('in cluster')
            else:
                # print('checking open set')
                if neigh in clusterOpenSet:
                    dontAddMe = True
                    # print('in open set')

            # print('checing closed set')
            if neigh in clusterClosedSet:
                # print('in closed set')
                dontAddMe = True

            if not dontAddMe:
                # print(neigh, 'addendum, doo')
                freeNeighborList.append(neigh)
        # initialize the return list
        returnList = []
        # print(allUnknown," <------ allUnknown*****")
        reallyGoodToReturn = aNode._cost == -1 and goodToReturn and not allUnknown
        if aNode._cost == -1 and goodToReturn and not allUnknown:
            # print('made it boi!!')
            # if the current node is unknown and has a known neighbor,
            # add it to be returned
            returnList.append(aNode)
            # print(returnList, reallyGoodToReturn, 'YEA BOIIIIII')
            return [returnList, reallyGoodToReturn, freeNeighborList]
        elif goodToReturn and not allUnknown:
            # print('did not make it boi :(')
            # print(freeNeighborList, reallyGoodToReturn)
            return [freeNeighborList, reallyGoodToReturn]
            # pass
        else:
            return [[], reallyGoodToReturn]


    def findNearestKnown(self,node, tolerance = .15):
        distance = 5000
        outputTuple = ()
        prior = Queue.PriorityQueue()
        # step through the closed set, this contains all the known nodes in the map
        for knownNode in self._clusterClosedSet:
            if knownNode._cost is not -1 and knownNode._cost < 70:
                # find the distance between the input node, and the node we're checking
                tempDistance = math.hypot(node._x_pos - knownNode._x_pos, node._y_pos - knownNode._y_pos)
                # if the distance is within tolerance, output the node
                # if tempDistance < tolerance:
                #     return knownNode
                # if this is the best distance, store it
                if tempDistance < distance:
                    distance = tempDistance
                    prior.put([tempDistance, knownNode])
        # return the best node
        return prior.get()[1]
