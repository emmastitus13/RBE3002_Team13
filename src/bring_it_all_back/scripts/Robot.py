#!/usr/bin/env python

import rospy, tf, copy, math
from math import cos, sin, atan2,  degrees, sqrt, pi
from kobuki_msgs.msg import BumperEvent
from geometry_msgs.msg import Twist, Pose, PoseStamped, PoseWithCovarianceStamped
from actionlib_msgs.msg import GoalStatusArray, GoalStatus
from tf.transformations import euler_from_quaternion, quaternion_from_euler
import numpy as np
from std_msgs.msg import String
import actionlib
from actionlib_msgs.msg import *
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal


class Robot:
    def __init__(self, wheelbase):
        self._wheelbase = wheelbase
        self._pose = Pose()
        self._x_pos = self._pose.position.x
        self._y_pos = self._pose.position.y
        quat = self._pose.orientation
        quat2 = [quat.x, quat.y, quat.z, quat.w]
        roll, pitch, yaw = euler_from_quaternion(quat2)
        self._roll = roll
        self._pitch = pitch
        self._yaw = yaw
        self._speed = 0.3
        self._odom = tf.TransformListener()
        self._status = 0
        # Timer to update robot pose
        rospy.Timer(rospy.Duration(.1, 0), self.timerCallback)
        # Robot motion publisher (Twist)
        self._tPub = rospy.Publisher('cmd_vel_mux/input/teleop', Twist, None, queue_size=10)
        # navPose subscriber (PoseStamped)
        self._gSub = rospy.Subscriber('move_base_simple/goal2', PoseStamped, self.navToPose, queue_size=1)
        self._drive = rospy.Publisher('move_base_simple/goal', PoseStamped, queue_size=1)
        self._checkStatus = rospy.Subscriber('move_base/status', GoalStatusArray, self.returnStatus, queue_size = 1)
        self._initialPosition = rospy.Publisher('initialpose', PoseWithCovarianceStamped, queue_size=1)
        self._move_base = actionlib.SimpleActionClient("move_base", MoveBaseAction)
        print("tmp")

    #returns the inital position of the robot
    def startingPosition(self):
        startPos = PoseWithCovarianceStamped()
        startPos.pose.pose.position.x = self._x_pos
        startPos.pose.pose.position.y = self._y_pos
        startPos.pose.pose.orientation = self._pose.orientation
        self._initialPosition.publish(startPos)
        return [self._x_pos, self._y_pos]

    # Drives straight a distance at a given speed
    # Stores the speed for future use
    def driveStraight(self, spd, dist):
        print 'starting straight!'
        origin = copy.deepcopy(self._pose)
        tm = Twist()
        tm.linear.x = spd
        self._speed = spd
        tm.angular.z = 0.
        done = False
        while not done and not rospy.is_shutdown():
            curr_dist = math.sqrt((self._pose.position.x-origin.position.x)**2 + (self._pose.position.y-origin.position.y)**2)
            # print(curr_dist,' <-- cur dist', dist,' <-- desired')
            if curr_dist >= dist:
                done = True
            self._tPub.publish(tm)
        tm.linear.x = 0.
        self._tPub.publish(tm)
        print 'done straight!'

    # Turns for an angle relative to current angle
    def rotate(self, angle):
        print 'starting turn!'
        tm = Twist()

        tm.linear.x = 0
        tm.linear.y = 0
        yaw = self._yaw

        if angle > math.pi:
            angle -= (2*math.pi)
        elif angle < -math.pi:
            angle += (2*math.pi)

        end = yaw + angle
        if end < -math.pi:
            end += (2*math.pi)
        elif end > math.pi:
            end -= (2*math.pi)

        error = end-self._yaw
        # print "Error: " + str(error) + " End " + str(end) + " Current " + str(self._yaw)
        while abs(error) >= .05 and not rospy.is_shutdown():
            tm.linear.x = 0
            tm.linear.y = 0
            # print "Error: " + str(error) + " End " + str(end) + " Current " + str(self._yaw)

            error = end - self._yaw

            if angle > 0:
                tm.angular.z = self._speed
            else:
                tm.angular.z = -self._speed
            self._tPub.publish(tm)
        tm.angular.z = 0
        self._tPub.publish(tm)
        print('Done turning!')

    # Turns and drives to an (x,y) point then turns to point to an orientation
    # Uses rotate() and driveStraight() functions
    def navToPose(self, goal):
        print "*************************************************************************"
        # transform the nav goal from the global coordinate system to the robot's coordinate system
        print "Current: x " + str(self._x_pos) + " y " + str(self._y_pos) + " theta " + str(self._yaw)

        # self._odom.waitForTransform('odom', 'base_footprint', rospy.Time(0), rospy.Duration(1.0))
        # transGoal = self._odom.transformPose('base_footprint', goal)
        # x_gol = transGoal.pose.position.x
        # y_gol = transGoal.pose.position.y
        # quat1 = transGoal.pose.orientation
        x_gol = goal.pose.position.x
        y_gol = goal.pose.position.y
        quat1 = goal.pose.orientation
        quat2 = [quat1.x, quat1.y, quat1.z, quat1.w]
        roll, pitch, yaw = euler_from_quaternion(quat2)
        th_gol = yaw
        # print "x " + str(x_gol) + " y " + str(y_gol) + " theta " + str(th_gol)
        print "Goal:    x " + str(goal.pose.position.x) + " y " + str(goal.pose.position.y) + " theta " + str(th_gol)

        init_x = self._x_pos
        init_y = self._y_pos

        # print "cur x " + str(init_x) + " cur y " + str(init_y) + " cur th " + str(self._yaw)

        manhat_x = x_gol-init_x
        manhat_y = y_gol-init_y

        dist = math.sqrt(manhat_x**2 + manhat_y**2)

        targ_ang = atan2(y_gol-init_y,x_gol-init_x)
        print "targ_ang " + str(targ_ang)
        ang = targ_ang-self._yaw

        if ang > math.pi:
            ang -= (2*math.pi)
        if ang < -math.pi:
            ang += (2*math.pi)

        print "dx " + str(manhat_x) + " dy " + str(manhat_y) + " dtheta " + str(ang)

        self.rotate(ang)
        self.driveStraight(0.2, dist)
        self.rotate(th_gol-self._yaw)

    #rotate around current position
    def rotate360(self):
        #self.rotate(math.pi*(2./3.))
        #self.rotate(math.pi*(2./3.))
        # self.rotate(math.pi*(2./3.))
        tm1 = Twist()
        tm1.linear.x = 0
        tm1.linear.y = 0
        tm1.linear.z = 0
        tm1.angular.x = 0
        tm1.angular.y = 0
        tm1.angular.z = .2

        tm2 = Twist()
        tm2.linear.x = 0
        tm2.linear.y = 0
        tm2.linear.z = 0
        tm2.angular.x = 0
        tm2.angular.y = 0
        tm2.angular.z = 0
        for i in range(0,400):
            print ('you spin me right round')
            self._tPub.publish(tm1)
            rospy.sleep(.1)
            print('i spun guys')
        self._tPub.publish(tm2)

    def returnStatus(self, event):
        #print(event.status_list)
        self._status = event.status_list
        return(self._status)

    def holdTillGoal(self):
        rospy.sleep(3)
        while not (self._status[1].status == 3):
            #print(self._status[1].status,'BANANANANANNA')
            if (self._status[1].status == 2):
                print('preempted: received cancel request after began executing')
                #break
                raise Exception('Preempted')
            if(self._status[1].status == 4):
                print('aborted: stopped after some failure')
                #break
                raise Exception('Aborted Finding Goal')
            if(self._status[1].status == 5):
                print('rejected unattainable goal')
                #break
                raise Exception('Rejected')
            if(self._status[1].status == 6):
                print('preempting: received canel request and hasnt stopped executing')
                #break
                raise Exception('Preempting')
            if(self._status[1].status == 7):
                print ('recalling: cancel request hasnt been confirmed')
                #break
                raise Exception('recalling')
            if(self._status[1].status == 8):
                print ('recalled: cancel request was successful')
                #break
                raise Exception('recalled')
            if(self._status[1].status == 9):
                print ('lost: where did the goal go? who knows?')
                #break
                raise Exception('Lost')

    def driveToGoal(self, grid, x, y):
        # goal = PoseStamped()
        # goal.header.frame_id = "map"
        # goal.header.stamp = rospy.Time()
        # pose = Pose()
        # [pose.position.x, pose.position.y] = grid.gridToMeters(x, y)
        # quat = quaternion_from_euler(0, 0, 0.9)
        # pose.orientation.x = quat[0]
        # pose.orientation.y = quat[1]
        # pose.orientation.z = quat[2]
        # pose.orientation.w = quat[3]
        # goal.target_pose.pose = pose
        #
        # self.drive.publish(goal)
        # finished_within_time = self._move_base.wait_for_result(rospy.Duration(300))
        #
        # if not finished_within_time:
        #     self._move_base.cancel_goal()
        #     print("yo dumb ass too slow")
        # else:
        #     state = self._move_base.get_state()
        #     if state == GoalStatus.SUCCEEDED:
        #         print("made it!")
        #         return True
        #     else:
        #         return False

        goal = MoveBaseGoal()
        goal.target_pose.header.frame_id = "map"
        goal.target_pose.header.stamp = rospy.Time()
        pose = Pose()
        [pose.position.x, pose.position.y] = grid.gridToMeters(x, y)
        quat = quaternion_from_euler(0, 0, 0.9)
        pose.orientation.x = quat[0]
        pose.orientation.y = quat[1]
        pose.orientation.z = quat[2]
        pose.orientation.w = quat[3]
        goal.target_pose.pose = pose

        self._move_base.send_goal(goal)
        finished_within_time = self._move_base.wait_for_result(rospy.Duration(300))

        if not finished_within_time:
            self._move_base.cancel_goal()
            print("yo dumb ass too slow")
        else:
            state = self._move_base.get_state()
            print (state, 'DRIVE STATE')
            if state == GoalStatus.SUCCEEDED:
                print("made it!")
                return True
            else:
                return False

    def timerCallback(self, event):
        """
            This is a callback that runs every 0.1s.
            Updates this instance of Robot's internal position variable (self._current)
        """
        self._odom.waitForTransform('map', 'base_footprint', rospy.Time(0), rospy.Duration(3.0))
        (position, orientation) = self._odom.lookupTransform('map', 'base_footprint', rospy.Time(0))
        self._pose.position.x = position[0]
        self._pose.position.y = position[1]

        self._x_pos = self._pose.position.x
        self._y_pos = self._pose.position.y

        self._pose.orientation.x = orientation[0]
        self._pose.orientation.y = orientation[1]
        self._pose.orientation.z = orientation[2]
        self._pose.orientation.w = orientation[3]
        q = [self._pose.orientation.x,
             self._pose.orientation.y,
             self._pose.orientation.z,
             self._pose.orientation.w]  # quaternion nonsense

        (roll, pitch, yaw) = euler_from_quaternion(q)
        self._roll = roll
        self._pitch = pitch
        self._yaw = yaw
        # for debugging, print the current position
        # print(self._pose.position.x, ' <-- cur x ', self._pose.position.y, ' <-- cur y')